namespace Enjoy.Lang.Compiler

open System.Collections.Generic
open System.IO
open System.Reflection

open CompilerResult.Computation
open Parser

type ExpressionsCollection =
    { GlobalNamespace: NamespaceTree
      Expressions: Expression list list }

module Collector =
    let collectOne preloadAssemblies parserResult =
        match parserResult.FilesToInclude with
        | _ :: _ -> Error $"不能包含其他文件"
        | [] ->
            let globalNamespace = NamespaceTree.getEmpty ()
            preloadAssemblies |> Seq.iter (NamespaceTree.loadAssembly globalNamespace)

            { GlobalNamespace = globalNamespace
              Expressions = [ parserResult.Expressions ] }
            |> Ok

    let collect (preloadAssemblies: _ seq) path =
        let globalNamespace = NamespaceTree.getEmpty ()
        preloadAssemblies |> Seq.iter (NamespaceTree.loadAssembly globalNamespace)
        let loadedAssemblies = HashSet(preloadAssemblies)

        let loadAssemblies parserResult =
            let chooser asmPath =
                let fullPath = Path.GetFullPath(asmPath)

                try
                    let asm = Assembly.Load(fullPath)

                    if loadedAssemblies.Contains(asm) then
                        None
                    else
                        NamespaceTree.loadAssembly globalNamespace asm
                        loadedAssemblies.Add(asm) |> ignore
                        None
                with exn ->
                    Some $"无法加载动态链接库{fullPath}: {exn.Message}"

            parserResult.AssembliesReferenced
            |> List.choose chooser
            |> List.map (fun msg -> CompilerMessage(NoSource, msg, None, Severity.Error))

        let importNamespaces parserResult =
            let chooser ns =
                match NamespaceTree.import globalNamespace ns with
                | Some types ->
                    types |> Seq.iter globalNamespace.Types.Add
                    None
                | None -> Some $"无法导入命名空间{ns}"

            parserResult.NamespacesImported
            |> List.choose chooser
            |> List.map (fun msg -> CompilerMessage(NoSource, msg, None, Severity.Error))

        let rec bfs visited exprs messages queue =
            match queue with
            | Queue.Cons(path, xs) ->
                let fullPath = Path.GetFullPath(path)

                if visited |> Set.contains fullPath then
                    Fault
                        [ CompilerMessage(
                              { Path = fullPath; Text = "" },
                              $"文件{fullPath}被循环引用",
                              None,
                              Severity.Error
                          ) ]
                else
                    let visited' = Set.add fullPath visited
                    let sourceText = File.ReadAllText(fullPath)
                    let source = { Path = fullPath; Text = sourceText }

                    compile {
                        let! parserResult = parse source

                        let messages' =
                            loadAssemblies parserResult @ importNamespaces parserResult
                            |> List.map (CompilerMessage.withSource source)
                            |> (@) messages

                        let exprs' = parserResult.Expressions :: exprs

                        let queue' =
                            (xs, parserResult.FilesToInclude)
                            ||> Seq.fold (fun q p -> Queue.enqueue p q)

                        return! bfs visited' exprs' messages' queue'
                    }
            | Queue.Nil ->
                Compiled(
                    { GlobalNamespace = globalNamespace
                      Expressions = exprs },
                    messages
                )

        bfs Set.empty [] [] (Queue.empty |> Queue.enqueue path)
