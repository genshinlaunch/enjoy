namespace Enjoy.Lang.Compiler

[<RequireQualifiedAccess>]
type Severity =
    | Info
    | Warning
    | Error

type CompilerMessage<'Source when 'Source :> ISource> =
    | CompilerMessage of source: 'Source * text: string * span: SourceSpan option * severity: Severity

module CompilerMessage =
    let isError message =
        match message with
        | CompilerMessage(_, _, _, Severity.Error) -> true
        | _ -> false

    let withSource (source: SourceUnit) (CompilerMessage(NoSource, text, span, severity)) =
        CompilerMessage(source, text, span, severity)

type CompilerMessage = CompilerMessage<SourceUnit>

type CompilerResult<'TResult, 'TSource when 'TSource :> ISource> =
    | Compiled of result: 'TResult * messages: CompilerMessage<'TSource> list
    | Fault of messages: CompilerMessage<'TSource> list

module CompilerResult =
    let bind f x =
        match x with
        | Compiled(result, messages) ->
            match f result with
            | Compiled(newResult, newMessages) -> Compiled(newResult, newMessages @ messages)
            | Fault newMessages -> Fault(newMessages @ messages)
        | Fault messages -> Fault messages

    let singleton x = Compiled(x, [])

    module Computation =
        type CompilationCombinator() =
            member _.Bind(x, f) = bind f x

            member _.Return(x) = singleton x

            member _.ReturnFrom(x) = x

        let compile = CompilationCombinator()

    let map mapping result =
        match result with
        | Compiled(result, msgs) -> Compiled(mapping result, msgs)
        | Fault msgs -> Fault msgs

    let satisfy pred ifFalse result =
        Computation.compile {
            let! result = result

            if pred result then
                return result
            else
                return! Fault(ifFalse result)
        }

    let sequence results =
        let rec loop results failed acc messages =
            match results with
            | [] ->
                if failed then
                    Fault messages
                else
                    Compiled(List.rev acc, messages)
            | Fault msgs :: results -> loop results true acc (messages @ msgs)
            | Compiled(result, msgs) :: results -> loop results failed (result :: acc) (messages @ msgs)

        loop results false [] []
