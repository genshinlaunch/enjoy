module Enjoy.Lang.Compiler.Emit

open System
open System.IO
open System.Reflection
open System.Reflection.Metadata
open System.Reflection.Metadata.Ecma335
open System.Reflection.PortableExecutable

let emitExpr metadataBuilder (il: InstructionEncoder) expr =
    match expr with
    | typed.Int value ->
        il.LoadConstantI4 value
    // | typed.Array elts ->


let writePEImage (peStream: Stream) metadataBuilder ilBuilder entryPoint contentId =
    let peHeaderBuilder = PEHeaderBuilder(imageCharacteristics = Characteristics.ExecutableImage)

    let peBuilder = ManagedPEBuilder(
        peHeaderBuilder,
        MetadataRootBuilder(metadataBuilder),
        ilBuilder,
        entryPoint = entryPoint,
        flags = CorFlags.ILOnly,
        deterministicIdProvider = fun _ -> contentId
    )
    
    let peBlob = BlobBuilder()
    peBuilder.Serialize(peBlob) |> ignore
    peBlob.WriteContentTo(peStream)

let emitDynamicAssembly name exprs outputPath =
    let guid = Guid()
    let contentId = BlobContentId(guid, 0x04030201u)

    let ilBuilder = BlobBuilder()
    let metadataBuilder = MetadataBuilder()

    let mainSignature = BlobBuilder()
    BlobEncoder(mainSignature)
        .MethodSignature()
        .Parameters(0, (fun returnType -> returnType.Void()), (fun _ -> ()))

    let methodBodyStream = MethodBodyStreamEncoder(ilBuilder)

    let flowBuilder = ControlFlowBuilder()
    let codeBuilder = BlobBuilder()
    let il = InstructionEncoder(codeBuilder, flowBuilder)

    List.iter (emitExpr metadataBuilder il) exprs

    let mainBodyOffset = methodBodyStream.AddMethodBody(il)

    let entryPoint =
        metadataBuilder.AddMethodDefinition(
            MethodAttributes.Public ||| MethodAttributes.Static ||| MethodAttributes.HideBySig,
            MethodImplAttributes.IL,
            metadataBuilder.GetOrAddString("Main"),
            metadataBuilder.GetOrAddBlob(mainSignature),
            mainBodyOffset,
            Unchecked.defaultof<ParameterHandle>
        )

    use peStream = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.ReadWrite)

    writePEImage peStream metadataBuilder ilBuilder entryPoint contentId
