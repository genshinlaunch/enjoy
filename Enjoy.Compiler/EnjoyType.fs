namespace Enjoy.Lang.Compiler

open System

open Util

[<RequireQualifiedAccess>]
type EnjoyType =
    | Nothing
    | CLI of Type
    | Object of Map<string, EnjoyType>
    | Function of EnjoyType list * EnjoyType list * EnjoyType
    | Generic of Type * EnjoyType list

    static member Void = CLI typeof<Void>

module EnjoyType =
    let private toEnjoyFunc (parameters: _[]) returns =
        let tryMakeGenericType (tp: Type) =
            if tp.IsGenericTypeDefinition then
                tp.MakeGenericType(parameters)
            else
                tp

        if returns = typeof<Void> then
            if parameters.Length > Symbols.actionTypes.Length then
                None
            else
                Some(tryMakeGenericType Symbols.actionTypes[parameters.Length])
        else if parameters.Length > Symbols.funcTypes.Length then
            None
        else
            Some(tryMakeGenericType Symbols.funcTypes[parameters.Length])

    let private toObjectType values =
        match values with
        | [||] -> Some typeof<obj>
        | _ ->
            if values.Length > Symbols.objTypes.Length then
                None
            else
                Some(Symbols.objTypes[values.Length].MakeGenericType(values))

    let rec toType tp =
        match tp with
        | EnjoyType.Nothing -> Some typeof<obj>
        | EnjoyType.CLI tp -> Some tp
        | EnjoyType.Object(entries: Map<string, EnjoyType>) ->
            entries
            |> Map.values
            |> Seq.map toType
            |> sequence
            |> Option.bind (Seq.toArray >> toObjectType)
        | EnjoyType.Function(required, notrequired, returns) ->
            maybe {
                let! returns = toType returns
                let! parameters = required @ notrequired |> mapO toType
                return! toEnjoyFunc (List.toArray parameters) returns
            }
        | EnjoyType.Generic(tp, args) ->
            if tp = typeof<Array> then
                match args with
                | [ x ] ->
                    maybe {
                        let! x = toType x
                        return x.MakeGenericType()
                    }
                | _ :: _ -> None
                | _ -> raise Assert.Unreachable
            else if tp.IsGenericTypeDefinition then
                try
                    args
                    |> mapO toType
                    |> Option.map (fun args -> tp.MakeGenericType(List.toArray args))
                with :? ArgumentException ->
                    None
            else
                None

    type private Resolving =
        | Namespace of NamespaceTree
        | Type of EnjoyType

    let ofTypeExpression nstree texpr =
        let getNsMember nstree name =
            match nstree.Types.TryGetValue(name) with
            | true, result -> result |> EnjoyType.CLI |> Type |> Some
            | false, _ ->
                match nstree.Children.TryGetValue(name) with
                | true, result -> Some(Namespace result)
                | false, _ -> None

        let chooseTypeOnly resolving =
            match resolving with
            | Type tp -> Some tp
            | Namespace _ -> None

        let rec resolve texpr =
            let resolveTypeOnly = resolve >> Option.bind chooseTypeOnly

            match texpr with
            | TypeExpression.Path(root, subsequence) ->
                let folder resolving segment =
                    maybe {
                        let! resolving = resolving

                        match segment with
                        | TypeSegment.Dot name ->
                            match resolving with
                            | Namespace ns -> return! getNsMember ns name
                            | Type(EnjoyType.CLI tp) ->
                                match tp.GetNestedType(name) with
                                | null -> return! None
                                | nestedType -> return nestedType |> EnjoyType.CLI |> Type
                            | _ -> return! None
                        | TypeSegment.Generic args ->
                            match resolving with
                            | Type(EnjoyType.CLI tp) ->
                                return!
                                    args
                                    |> mapO resolveTypeOnly
                                    |> Option.map (fun args -> EnjoyType.Generic(tp, args) |> Type)
                            | _ -> return! None
                    }

                List.fold folder (getNsMember nstree root) subsequence
            | TypeExpression.Function(required, notrequired, returns) ->
                match
                    mapO resolveTypeOnly required, mapO resolveTypeOnly notrequired, Option.map resolveTypeOnly returns
                with
                | Some required, Some notrequired, Some returns ->
                    EnjoyType.Function(required, notrequired, returns |> Option.defaultValue EnjoyType.Void)
                    |> Type
                    |> Some
                | _ -> None
            | TypeExpression.Object entries ->
                let mapping (key, value) =
                    match resolveTypeOnly value, key with
                    | Some(EnjoyType.Function(_, _, _)), Var _ -> None
                    | Some(EnjoyType.Function(_, _, _) as value), Func name
                    | Some value, Var name -> Some(name, value)
                    | _ -> None

                entries |> mapO mapping |> Option.map (Map >> EnjoyType.Object >> Type)

        texpr |> resolve |> Option.bind chooseTypeOnly
