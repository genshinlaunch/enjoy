namespace Enjoy.Lang.Compiler

[<RequireQualifiedAccess>]
type Operator =
    // 算数运算符.
    | Add
    | Sub
    | Mul
    | Div
    | Mod
    | Pow

    // 比较运算符.
    | Eq
    | Ne
    | Lt
    | Le
    | Gt
    | Ge

    // 逻辑运算符.
    | And
    | Or

type VarOrFunc =
    | Var of name: string
    | Func of name: string

    override this.ToString() =
        match this with
        | Var name -> "#" + name
        | Func name -> "@" + name

    member this.Name =
        match this with
        | Var name -> name
        | Func name -> name

[<RequireQualifiedAccess>]
type TypeExpression =
    | Path of root: string * sebsequence: TypeSegment list
    | Function of required: TypeExpression list * notrequired: TypeExpression list * returns: TypeExpression option
    | Object of entries: (VarOrFunc * TypeExpression) list

and [<RequireQualifiedAccess>] TypeSegment =
    | Generic of args: TypeExpression list
    | Dot of name: string

type Parameter = Parameter of name: VarOrFunc * ``type``: TypeExpression

[<RequireQualifiedAccess>]
type ExpressionData =
    | Array of elts: Expression list
    | Assign of recursive: bool * target: ExprPath * value: Expression
    | AugAssign of target: ExprPath * op: Operator * value: Expression
    | BinOp of left: Expression * op: Operator * right: Expression
    | Bool of value: bool
    | Break
    | Bytes of value: byte[]
    | Cast of value: Expression * ``type``: TypeExpression
    | CodeBlock of body: Expression list
    | Continue
    | Float of value: float
    | For of init: Expression * test: Expression * step: Expression * body: Expression list
    | ForEach of target: VarOrFunc * iter: Expression * body: Expression list
    | If of test: Expression * body: Expression list * orelse: Expression list
    | Int of value: int
    | List of elts: Expression list
    | Not of value: Expression
    | Nothing
    | Object of entries: (VarOrFunc * Expression) list
    | Path of path: ExprPath
    | Lambda of
        parameters: Parameter list *
        defparams: (Parameter * Expression) list *
        desc: string option *
        returns: TypeExpression option *
        body: Expression list
    | Repeat of times: Expression * body: Expression list
    | Return of value: Expression
    | String of value: string
    | Switch of subject: Expression * Cases: SwitchCase list * def: Expression list
    | Tag of name: string * attrs: (string * Expression) list * children: Expression list
    | Throw of exn: Expression
    | Try of body: Expression list * handler: ExceptionHandler * finalbody: Expression list
    | Until of test: Expression * body: Expression list
    | While of test: Expression * body: Expression list

and ExceptionHandler = ExceptionHandler of target: string * body: Expression list

and [<RequireQualifiedAccess>] SwitchValue =
    | Bool of bool
    | Float of float
    | Int of int
    | String of string

and SwitchCase = SwitchCase of value: SwitchValue * body: Expression list

and [<RequireQualifiedAccess>] PathSegment =
    | Var of name: string
    | Func of name: string
    | Int of index: int
    | Index of expr: Expression list
    | Call of args: Args
    | CallMember of memberName: string * args: Args

and Args = Args of posargs: Expression list * kwargs: (VarOrFunc * Expression) list

and ExprPath = ExprPath of root: VarOrFunc * subsequence: PathSegment list

and Expression =
    { Data: ExpressionData
      SourceSpan: SourceSpan }

type untyped = ExpressionData
