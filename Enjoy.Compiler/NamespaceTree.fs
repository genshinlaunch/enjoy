namespace Enjoy.Lang.Compiler

open System
open System.Collections.Generic
open System.Reflection

type NamespaceTree =
    { Children: IDictionary<string, NamespaceTree>
      Types: IDictionary<string, Type> }

module NamespaceTree =
    let getEmpty () =
        { Children = Dictionary()
          Types = Dictionary() }

    let loadAssembly tree (asm: Assembly) =
        let folder tree name =
            match tree.Children.TryGetValue(name) with
            | true, result -> result
            | false, _ ->
                let emptyTree = getEmpty ()
                tree.Children.Add(name, emptyTree)
                emptyTree

        for tp in asm.ExportedTypes do
            let ns = tp.Namespace.Split('.') |> Array.fold folder tree
            ns.Types.Add(tp.Name, tp)

    let rec import tree ns =
        match ns with
        | [] -> Some tree.Types
        | x :: xs ->
            match tree.Children.TryGetValue(x) with
            | true, result -> import result xs
            | false, _ -> None
