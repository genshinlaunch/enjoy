module Enjoy.Lang.Compiler.Parser

open FParsec

open Util

open System
open System.Globalization
open System.IO

type ParserResult =
    { FilesToInclude: string list
      NamespacesImported: string list list
      AssembliesReferenced: string list
      Aliases: (string * TypeExpression) list
      Expressions: Expression list }

module ParseResult =
    let empty =
        { FilesToInclude = []
          NamespacesImported = []
          AssembliesReferenced = []
          Aliases = []
          Expressions = [] }

type NumberLiteral with

    member this.Suffix =
        let chars =
            [ this.SuffixChar1; this.SuffixChar2; this.SuffixChar3; this.SuffixChar4 ]
            |> List.take this.SuffixLength

        String.Join("", chars)

let private positionToSourceLocation (position: Position) =
    { Line = int position.Line
      Column = int position.Column
      Index = int position.Index }

let private makeSourceSpan formerPosition latterPosition =
    { Start = positionToSourceLocation formerPosition
      End = positionToSourceLocation latterPosition }

let private withSourceSpan parser : Parser<Expression, _> =
    parse {
        let! formerPosition = getPosition
        let! result = parser
        let! latterPosition = getPosition

        return
            { Data = result
              SourceSpan = makeSourceSpan formerPosition latterPosition }
    }

let private reportError formerPosition text severity : Parser<_, CompilerMessage<NoSource> list> =
    parse {
        let! latterPosition = getPosition
        let span = makeSourceSpan formerPosition latterPosition
        let! messages = getUserState

        do!
            CompilerMessage(NoSource, text, (Some span), severity) :: messages
            |> setUserState

        return ()
    }

let private spaces = regex @"[\s,，]*([;；]{2}[\s\S]*[;；]{2}|[;；].*|[\s,，]*)*" >>% ()

let private spaces' =
    notFollowedBy (satisfy (fun c -> Char.IsLetterOrDigit(c) || c = '_')) .>> spaces

module internal Tokens =
    let lquotec = skipString "“"
    let lquotee = skipString "\""
    let rquotec = skipString "”"
    let rquotee = skipString "\""
    let sign (chars: string) = anyOf chars >>. spaces <?> chars[..0]
    let keyword s = skipString s >>. spaces' <?> s
    let lbracket = sign "[【"
    let rbracket = sign "]】"
    let lbracketbar = pstring "[|" <|> pstring "【|" >>. spaces <?> "[|"
    let rbracketbar = pstring "|]" <|> pstring "|】" >>. spaces <?> "|]"
    let lbrace = sign "{｛"
    let rbrace = sign "}｝"
    let lparen = sign "(（"
    let rparen = sign ")）"
    let ltag = sign "《"
    let rtag = sign "》"
    let at = sign "@"
    let dot = sign "、"

    let operator eng chn result =
        skipString eng >>. spaces <|> (regex chn >>. spaces') >>% result <?> eng

    let colon = anyOf ":：" >>. spaces <|> (skipString "设为" >>. spaces') >>% ()
    let assign = operator "=" "为" ()
    let question = pchar '?' >>. spaces
    let arrow = pstring "=>" >>. spaces
    let addeq = operator "+=" "自加|加等" Operator.Add
    let subeq = operator "-=" "自减|减等" Operator.Sub
    let muleq = operator "*=" "自乘|乘等" Operator.Mul
    let diveq = operator "/=" "自除|除等" Operator.Div
    let modeq = operator "%=" "自模|模等" Operator.Mod
    let poweq = stringReturn "**=" Operator.Pow .>> spaces
    let add = operator "+" "加" Operator.Add
    let sub = operator "-" "减" Operator.Sub
    let mul = operator "*" "乘" Operator.Mul
    let div = operator "/" "除" Operator.Div
    let mod' = operator "%" "模" Operator.Mod
    let pow = stringReturn "**" Operator.Pow .>> spaces
    let eq = operator "==" "等于" Operator.Eq

    let ne =
        ([ "!="; "<>" ] |> List.map skipString |> choice >>. spaces)
        <|> (skipString "不等于" >>. spaces')
        >>% Operator.Ne
        <?> "<>"

    let lt = operator "<" "小于" Operator.Lt
    let gt = operator ">" "大于" Operator.Gt
    let le = operator "<=" "小于等于" Operator.Le
    let ge = operator ">=" "大于等于" Operator.Ge
    let and' = operator "&&" "且" Operator.And
    let or' = operator "||" "或" Operator.Or
    let not' = keyword "取反"
    let async = keyword "异步"
    let if' = keyword "如果"
    let elif' = keyword "再则"
    let else' = keyword "否则"
    let while' = keyword "当"
    let do' = keyword "执行"
    let until = keyword "直到"
    let repeat = keyword "重复"
    let foreach = keyword "遍历"
    let try' = keyword "尝试"
    let catch = keyword "排查"
    let finally' = keyword "例行"
    let throw = keyword "抛出"
    let return' = keyword "返回"
    let break' = keyword "跳出"
    let continue' = keyword "继续"
    let switch = keyword "匹配"
    let default' = keyword "默认"
    let await = keyword "等待"
    let import = keyword "导入"
    let delegate' = keyword "委托"
    let reference = keyword "引用"
    let include' = keyword "包含"
    let alias = keyword "别名"
    let nothing = keyword "空"

open Tokens

#nowarn "40"

module internal Parsers =
    let parseExprNoLabel, parseExprRef = createParserForwardedToRef ()

    let parseExpr = parseExprNoLabel <?> "表达式"

    let parseBlock = lbrace >>. many parseExpr .>> rbrace .>> spaces

    let parseStringContent =
        let unescapedChar = noneOf "\\\"”"

        let escapedChar =
            [ ("\\\"", '"')
              ("\\”", '”')
              ("\\\\", '\\')
              ("\\/", '/')
              ("\\b", '\b')
              ("\\f", '\f')
              ("\\n", '\n')
              ("\\r", '\r')
              ("\\t", '\t') ]
            |> List.map (fun (toMatch, result) -> stringReturn toMatch result)
            |> choice

        let unicodeChar =
            parse {
                let! m = regex @"\\u(\d{4})"
                return Int32.Parse(m[1..], NumberStyles.HexNumber) |> char
            }

        let oneChar = choice [ unescapedChar; escapedChar; unicodeChar ]

        manyChars oneChar

    let parseQuotedString =
        parseStringContent |> between lquotec rquotec
        <|> (parseStringContent |> between lquotee rquotee)
        .>> spaces

    let parseLiteralNoBinary =
        let parseNumber =
            parse {
                let! formerPosition = getPosition

                let! number =
                    numberLiteral (NumberLiteralOptions.AllowMinusSign ||| NumberLiteralOptions.AllowFraction) "数字"

                do! spaces'

                if number.IsInteger then
                    match Int32.TryParse(number.String) with
                    | true, i -> return untyped.Int i
                    | false, _ ->
                        do! reportError formerPosition "无效的整数字面量" Severity.Error
                        return untyped.Nothing
                else
                    match Double.TryParse(number.String) with
                    | true, f -> return untyped.Float f
                    | false, _ ->
                        do! reportError formerPosition "无效的浮点字面量" Severity.Error
                        return untyped.Nothing
            }

        let parseBool =
            (stringReturn "真" true) <|> (stringReturn "假" false) |>> untyped.Bool
            .>> spaces'

        let parseString = parseQuotedString |>> untyped.String

        [ parseNumber; parseBool; parseString ] |> choice <?> "常量"

    let parseLiteral =
        let parseBinary =
            regex @"0[xX]([\da-fA-F]+)" .>> spaces'
            |>> (stringSkip 2 >> hexStringToBytes >> untyped.Bytes)

        parseBinary <|> parseLiteralNoBinary <?> "常量"

    let parseNothing = nothing >>% untyped.Nothing

    let parseVarRaw = regex @"#([\w`]+)" |>> stringSkip 1
    let parseFuncRaw = regex @"@([\w`]+)" |>> stringSkip 1

    let parseVar = parseVarRaw |>> Var
    let parseFunc = parseFuncRaw |>> VarOrFunc.Func
    let parseVarOrFunc = parseVar <|> parseFunc <?> "变量名(#...或@...)"

    let parseArgs =
        let rec loop posargs kwargs =
            parse {
                let! formerPosition = getPosition

                match! opt (parseVarOrFunc .>> spaces .>> assign .>> notFollowedBy (pchar '=') |> attempt) with
                | Some key when kwargs |> Seq.map fst |> Seq.contains key ->
                    do! reportError formerPosition "重复的关键字参数" Severity.Error
                    return! loop posargs kwargs
                | Some key ->
                    let! value = parseExpr
                    return! loop posargs ((key, value) :: kwargs)
                | None ->
                    match! opt parseExpr with
                    | Some _ when kwargs |> List.isEmpty |> not ->
                        do! reportError formerPosition "关键字参数必须在位置参数之后" Severity.Error
                        return! loop posargs kwargs
                    | Some posarg -> return! loop (posarg :: posargs) kwargs
                    | None -> return Args(List.rev posargs, List.rev kwargs)
            }

        loop [] []

    let parsePath =
        let parseSegment =
            let varOrInt (s: string) =
                match System.Int32.TryParse(s) with
                | true, i -> PathSegment.Int i
                | false, _ -> PathSegment.Var s

            // 路径中的右方括号不能接受尾随空白.
            let rbracket' = anyOf "]］" <?> "]"

            let parseFuncOrCallMember =
                parseFuncRaw
                .>>. opt (spaces >>. lbracket >>. parseArgs .>> rbracket' |> attempt)
                |>> (function
                | memberName, Some args -> PathSegment.CallMember(memberName, args)
                | func, None -> PathSegment.Func func)

            [ parseVarRaw |>> varOrInt
              parseFuncOrCallMember
              skipString "#" >>. lbracket >>. many parseExpr .>> rbracket'
              |>> PathSegment.Index
              lbracket >>. parseArgs |>> PathSegment.Call .>> rbracket' ]
            |> choice

        let parseSegments =
            many parseSegment .>>. opt (dot >>. parseExpr)
            |>> (function
            | segs, None -> segs
            | init, Some last -> init @ [ PathSegment.Call(Args([ last ], [])) ])

        parseVarOrFunc .>>. parseSegments .>> spaces' |>> ExprPath <?> "路径"

    let parseCodeBlock = parseBlock |>> untyped.CodeBlock

    let parseList = lbracket >>. many parseExpr .>> rbracket |>> untyped.List <?> "列表"

    let parseArray =
        lbracketbar >>. many parseExpr .>> rbracketbar |>> ExpressionData.Array <?> "数组"

    let parseObject =
        let rec loop entries =
            parse {
                let! formerPosition = getPosition

                match! opt (parseVarOrFunc .>> spaces .>> colon) with
                | Some key when entries |> Seq.map fst |> Seq.contains key ->
                    do! reportError formerPosition "重复的对象键" Severity.Error
                    return! loop entries
                | Some key ->
                    let! value = parseExpr
                    return! loop ((key, value) :: entries)
                | None -> return ExpressionData.Object(List.rev entries)
            }

        attempt (at >>. lbrace) >>. loop [] .>> rbrace <?> "对象"

    let parseType =
        let parseType, parseTypeRef = createParserForwardedToRef ()

        let parseTypePath =
            let parseTypeSegment =
                parseVarRaw |>> TypeSegment.Dot
                <|> (many parseType |> between lbracket (anyOf "]]") |>> TypeSegment.Generic)

            parseVarRaw .>>. many parseTypeSegment |>> TypeExpression.Path

        let parseFunctionType =
            parse {
                let! required = attempt (at >>. lbracket) >>. many parseType
                let! notrequired = many (question >>. parseType) .>> rbracket
                let! returns = opt (colon >>. parseType)
                return TypeExpression.Function(required, notrequired, returns)
            }

        let parseObjectType =
            attempt (at >>. lbrace) >>. many (parseVarOrFunc .>> colon .>>. parseType)
            .>> rbrace
            |>> TypeExpression.Object

        parseTypeRef.Value <- [ parseTypePath; parseFunctionType; parseObjectType ] |> choice

        parseType <?> "类型"

    let parseLambda =
        let parseParam = parseVarOrFunc .>> spaces .>> colon .>>. parseType |>> Parameter

        let rec parseParams parameters defparams =
            parse {
                let! formerPosition = getPosition

                match! opt parseParam with
                | Some key when defparams |> Seq.map fst |> Seq.append parameters |> Seq.contains key ->
                    do! reportError formerPosition "重复的参数" Severity.Error
                    return! parseParams parameters defparams
                | Some key ->
                    match! opt (assign >>. parseExpr) with
                    | Some def -> return! parseParams parameters ((key, def) :: defparams)
                    | None when defparams |> List.isEmpty |> not ->
                        do! reportError formerPosition "可选参数必须在必须参数之后" Severity.Error
                        return! parseParams parameters defparams
                    | None -> return! parseParams (key :: parameters) defparams
                | None ->
                    let! desc = opt parseQuotedString
                    return List.rev parameters, List.rev defparams, desc
            }


        parse {
            do! attempt (at >>. lbracket)
            let! parameters, defparams, desc = parseParams [] []
            let! returns = rbracket >>. opt (colon >>. parseType)
            let! body = parseBlock
            return untyped.Lambda(parameters, defparams, desc, returns, body)
        }

    let parseTag =
        let rec loop acc =
            parse {
                let! formerPosition = getPosition

                match! opt (parseVarRaw .>> spaces .>> assign) with
                | Some key when acc |> Seq.map fst |> Seq.contains key ->
                    do! reportError formerPosition "重复的标签属性" Severity.Error
                    return acc, []
                | Some key ->
                    let! value = parseExpr
                    return! loop ((key, value) :: acc)
                | None ->
                    let! children =
                        opt (spaces >>. (many parseExpr |> between lbracket rbracket))
                        |>> Option.defaultValue []

                    return List.rev acc, children
            }

        ltag >>. regex @"\w+" .>> spaces .>>. loop [] .>> rtag
        |>> (fun (name, (attrs, children)) -> untyped.Tag(name, attrs, children))
        <?> "标签"

    let parseParen = lparen >>. parseExpr .>> rparen

    let parseAssignmentTarget =
        parse {
            let! formerPosition = getPosition

            match! parsePath with
            | ExprPath(_, []) as path -> return path
            | ExprPath(_, subsequence) as path when
                (match List.last subsequence with
                 | PathSegment.Call _
                 | PathSegment.CallMember(_, _) -> true
                 | _ -> false)
                ->
                do! reportError formerPosition "不能对函数调用赋值" Severity.Error
                return path
            | path -> return path
        }

    let parseAssign =
        let augops = [ addeq; subeq; muleq; diveq; modeq; poweq ] |> choice

        parse {
            let! target = parseAssignmentTarget

            match! opt augops with
            | Some augop ->
                let! value = parseExpr
                return untyped.AugAssign(target, augop, value)
            | None ->
                let! recursive = colon >>% false <|> (assign >>% true)
                let! value = parseExpr
                return untyped.Assign(recursive, target, value)
        }
        |> attempt

    let parseIf =
        parse {
            let! test = if' >>. parseExpr
            let! body = parseBlock

            let! elifs =
                many
                <| parse {
                    let! formerPosition = getPosition
                    let! test = elif' >>. parseExpr
                    let! body = parseBlock
                    let! latterPosition = getPosition
                    return makeSourceSpan formerPosition latterPosition, test, body
                }

            let! orelse = opt (else' >>. parseBlock) |>> Option.defaultValue []

            let orelse =
                let folder orelse (span, test, body) =
                    [ { Data = untyped.If(test, body, orelse)
                        SourceSpan = span } ]

                List.fold folder orelse elifs

            return untyped.If(test, body, orelse)
        }

    let parseForOrWhile =
        parse {
            let! testorinit = while' >>. parseExpr
            // "{...}"既可以是CodeBlock字面量, 又可以是语法结构的组成部分.
            match! parseExpr with
            | { Data = untyped.CodeBlock(body) } ->
                let test = testorinit
                return untyped.While(test, body)
            | test ->
                let init = testorinit
                let! step = parseExpr
                let! body = parseBlock
                return untyped.For(init, test, step, body)
        }

    let parseUntil =
        do' >>. parseBlock .>> until .>>. parseExpr |>> (swap >> untyped.Until)

    let parseRepeat = repeat >>. parseExpr .>>. parseBlock |>> untyped.Repeat

    let parseForeach =
        parse {
            let! formerPosition = getPosition
            do! foreach

            match! parseExpr with
            | { Data = untyped.Assign(true, iter, { Data = untyped.Path(ExprPath(target, [])) }) } ->
                let! body = parseBlock
                let! latterPosition = getPosition

                return
                    untyped.ForEach(
                        target,
                        { Data = untyped.Path iter
                          SourceSpan = makeSourceSpan formerPosition latterPosition },
                        body
                    )
            | iter ->
                do! assign
                let! target = parseVarOrFunc .>> spaces
                let! body = parseBlock
                return untyped.ForEach(target, iter, body)
        }

    let parseTry =
        let f (((body, target), handlerbody), finalbody) =
            untyped.Try(body, ExceptionHandler(target, handlerbody), finalbody |> Option.defaultValue [])

        try' >>. parseBlock .>> catch .>>. parseVarRaw .>> spaces
        .>>. parseBlock
        .>>. opt (finally' >>. parseBlock)
        |>> f

    let parseThrow = throw >>. parseExpr |>> untyped.Throw
    let parseReturn = return' >>. parseExpr |>> untyped.Return
    let parseBreak = break' >>% untyped.Break
    let parseContinue = continue' >>% untyped.Continue

    let parseSwitch =
        let parseSwitchValue =
            parseLiteralNoBinary
            |>> function
                | untyped.Int i -> SwitchValue.Int i
                | untyped.Float f -> SwitchValue.Float f
                | untyped.String s -> SwitchValue.String s
                | untyped.Bool b -> SwitchValue.Bool b
                | _ -> raise Assert.Unreachable

        let rec loop cases =
            parse {
                match! opt parseSwitchValue with
                | Some value ->
                    let! body = parseBlock
                    return! loop (SwitchCase(value, body) :: cases)
                | None ->
                    let! def = opt (default' >>. parseBlock)
                    return List.rev cases, def |> Option.defaultValue []
            }

        parse {
            let! subject = switch >>. parseExpr .>> lbrace
            let! cases, def = loop [] .>> rbrace
            return untyped.Switch(subject, cases, def)
        }

    let parseAtom =
        [ parsePath |>> untyped.Path
          parseLiteral
          parseList
          parseTag
          parseObject
          parseCodeBlock ]
        |> choice
        |> withSourceSpan
        <|> parseParen

    let parseCast =
        parse {
            let! formerPosition = getPosition
            let! atom = parseAtom

            match! opt (arrow >>. parseType) with
            | Some ``type`` ->
                let! latterPosition = getPosition

                return
                    { Data = untyped.Cast(atom, ``type``)
                      SourceSpan = makeSourceSpan formerPosition latterPosition }
            | _ -> return atom
        }

    let parseFactor =
        parseAtom <|> (not' >>. parseCast |>> untyped.Not |> withSourceSpan)

    let parseBinOp =
        let makeBinOpParser operand ops =
            let maybeOpAndRight = choice ops .>>. operand |> opt

            let rec loop acc =
                parse {
                    let! formerPosition = getPosition

                    match! maybeOpAndRight with
                    | Some(op, right) ->
                        let! latterPosition = getPosition
                        let sourceSpan = makeSourceSpan formerPosition latterPosition

                        return!
                            loop
                                { Data = untyped.BinOp(acc, op, right)
                                  SourceSpan = sourceSpan }
                    | None -> return acc
                }

            operand >>= loop

        let parsePow = makeBinOpParser parseFactor [ pow ]
        let parseTerm = makeBinOpParser parsePow [ mul; div; mod' ]
        let parseArith = makeBinOpParser parseTerm [ add; sub ]
        let parseComp = makeBinOpParser parseArith [ eq; ne; le; ge; lt; gt ]
        let parseAnd = makeBinOpParser parseComp [ and' ]

        makeBinOpParser parseAnd [ or' ]

    parseExprRef.Value <-
        [ parseAssign
          parseLambda
          parseIf
          parseForOrWhile
          parseUntil
          parseRepeat
          parseForeach
          parseTry
          parseThrow
          parseReturn
          parseBreak
          parseContinue
          parseSwitch ]
        |> choice
        |> withSourceSpan
        <|> parseBinOp

    let parseImport =
        import >>. many parseFuncRaw .>> spaces'
        |>> (fun names result ->
            { result with
                NamespacesImported = names :: result.NamespacesImported })

    let parseReference =
        reference >>. parseQuotedString .>> spaces
        |>> (fun path result ->
            { result with
                AssembliesReferenced = path :: result.AssembliesReferenced })

    let parseInclude =
        include' >>. parseQuotedString .>> spaces
        |>> (fun path result ->
            { result with
                FilesToInclude = path :: result.FilesToInclude })

    let parseAlias =
        alias >>. parseVarRaw .>> assign .>>. parseType
        |>> (fun alias result ->
            { result with
                Aliases = alias :: result.Aliases })

    let parseTopLevelExpr =
        parseExpr
        |>> (fun expr result ->
            { result with
                Expressions = expr :: result.Expressions })

    let parseTopLevel =
        [ parseImport; parseReference; parseInclude; parseAlias; parseTopLevelExpr ]
        |> choice

    let parseModule =
        let rec loop result =
            parse {
                match! opt eof with
                | Some() ->
                    return
                        { FilesToInclude = result.FilesToInclude |> List.distinct |> List.rev
                          NamespacesImported = result.NamespacesImported |> List.distinct
                          AssembliesReferenced = result.AssembliesReferenced |> List.distinct
                          Aliases = result.Aliases
                          Expressions = result.Expressions |> List.rev }
                | None ->
                    let! toplevel = parseTopLevel
                    return! result |> toplevel |> loop
            }

        spaces >>. loop ParseResult.empty

let private formatFParsecError (error: ParserError) =
    let dummyPositionPrinter _ _ _ _ = ()
    use sw = new StringWriter()
    error.WriteTo(sw, dummyPositionPrinter)
    string sw

let parse source =
    match runParserOnString Parsers.parseModule [] "" source.Text with
    | Success(result, messages, _) ->
        let messages = messages |> List.map (CompilerMessage.withSource source)

        if messages |> List.exists CompilerMessage.isError then
            Compiled(result, messages)
        else
            Fault messages
    | Failure(_, error, messages) ->
        let position = error.Position

        let span =
            { Start = positionToSourceLocation position
              End =
                { Line = int position.Line
                  Column = int position.Column + 1
                  Index = int position.Index + 1 } }

        CompilerMessage(NoSource, formatFParsecError error, Some span, Severity.Error)
        :: messages
        |> List.map (CompilerMessage.withSource source)
        |> Fault
