// Functional Algorithm Verified, P218

namespace Enjoy.Lang.Compiler

type internal QueueStatus<'T> =
    | Idle
    | Rev of int * 'T list * 'T list * 'T list * 'T list
    | App of int * 'T list * 'T list
    | Done of 'T list

type Queue<'T> =
    internal
        { Front: 'T list
          FrontLength: int
          Status: QueueStatus<'T>
          Rear: 'T list
          RearLength: int }

module Queue =
    let private exec status =
        match status with
        | Rev(ok, x :: f, f', y :: r, r') ->
            Rev(ok + 1, f, x :: f', r, y :: r')
        | Rev(ok, [], f', [ y ], r') ->
            App(ok, f', y :: r')
        | App(0, f', r') -> Done r'
        | App(ok, x :: f', r') -> App(ok - 1, f', x :: r')
        | s -> s

    let private invalidate status =
        match status with
        | Rev(ok, f, f', r, r') -> Rev(ok - 1, f, f', r, r')
        | App(0, f', _ :: r') -> Done r'
        | App(ok, f', r') -> App(ok - 1, f', r')
        | s -> s

    let private exec2 q =
        match exec (exec q.Status) with
        | Done fr -> { q with Status = Idle; Front = fr }
        | newstatus -> { q with Status = newstatus }

    let private check q =
        if q.RearLength <= q.FrontLength then
            exec2 q
        else
            let newstate = Rev(0, q.Front, [], q.Rear, [])
            exec2 { q with FrontLength = q.FrontLength + q.RearLength; Status = newstate; Rear = []; RearLength = 0 }

    let empty<'a> = { Front = list<'a>.Empty; FrontLength = 0; Status = Idle; Rear = list<'a>.Empty; RearLength = 0 }

    let tryPeek queue =
        match queue.Front with
        | [] -> None
        | x :: _ -> Some x

    let peek queue =
        match queue.Front with
        | [] -> raise (System.ArgumentException())
        | x :: _ -> x
    
    let enqueue x queue =
        { queue with Rear = x :: queue.Rear; RearLength = queue.RearLength + 1 }
        |> check

    let dequeue queue =
        { queue with FrontLength = queue.FrontLength - 1; Front = List.tail queue.Front; Status = invalidate queue.Status }
        |> check

    let isEmpty queue = queue.FrontLength = 0

    let (|Cons|Nil|) queue =
        match queue with
        | { Front = x :: _ } -> Cons(x, dequeue queue)
        | _ -> Nil
