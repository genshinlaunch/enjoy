namespace Enjoy.Lang.Compiler

type ISource =
    interface
    end

type SourceLocation = { Line: int; Column: int; Index: int }

type SourceSpan =
    { Start: SourceLocation
      End: SourceLocation }

module SourceSpan =
    let zero =
        let loc = { Line = 1; Column = 1; Index = 1 }
        { Start = loc; End = loc }

type SourceUnit =
    { Path: string
      Text: string }

    interface ISource

type NoSource =
    | NoSource

    interface ISource
