[<RequireQualifiedAccess>]
module Enjoy.Lang.Compiler.Symbols

open System
open System.Collections

open Enjoy.Lang.Runtime

let toString = typeof<obj>.GetMethod("ToString")
let count = typeof<ICollection>.GetMethod("get_Count")
let length = typeof<string>.GetMethod("get_Length")

let makeTag = typeof<Tag<_>>.GetConstructors()[0]

let private runtimeAsm = typeof<Tag<_>>.Assembly

let funcTypes = [| for i in 1..16 -> runtimeAsm.GetType($"IEnjoyFunc`{i}") |]

let actionTypes =
    [| yield runtimeAsm.GetType("IEnjoyAction")
       for i in 1..15 -> runtimeAsm.GetType($"IEnjoyAction`{i}") |]

let funcInvokes = funcTypes |> Array.map _.GetMethod("Invoke")

let actionInvokes = actionTypes |> Array.map _.GetMethod("Invoke")

let defaultGetters =
    let defaultType = runtimeAsm.GetType("IEnjoyDefaultFunc`15")
    [| for i in 1..16 -> defaultType.GetMethod($"get_Default{i}") |]

let objTypes = [| for i in 0..16 -> runtimeAsm.GetType($"Object`{i}") |]
