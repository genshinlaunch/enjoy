module Enjoy.Lang.Compiler.TypeChecker

open System
open System.Collections.Generic

open CompilerResult.Computation
open Util

type TypeCheckerScope =
    { Parent: TypeCheckerScope option
      Names: IDictionary<string, EnjoyType> }

type TypeCheckerState =
    { Scope: TypeCheckerScope
      GlobalNamespace: NamespaceTree }

let rec private lookupName name scope =
    match scope.Names.TryGetValue(name) with
    | true, result -> Some(scope.Parent = None, result)
    | false, _ -> scope.Parent |> Option.bind (lookupName name)

let private ifTypeMismatch expected got =
    [ CompilerMessage(NoSource, $"类型不匹配. 应为{expected}, 而给定的是{got.Type}", Some got.SourceSpan, Severity.Error) ]

let rec private checkExpression state (expr: Expression) : CompilerResult<TypedExpression, NoSource> =
    match expr.Data with
    | untyped.Array elts -> checkArray state expr.SourceSpan elts
    | untyped.Assign(recursive, target, value) -> checkAssign state expr.SourceSpan recursive target value
    | untyped.AugAssign(target, op, value) -> checkAugAssign state expr.SourceSpan target op value
    | untyped.BinOp(left, op, right) -> checkBinOp state expr.SourceSpan left op right
    | untyped.Bool value -> checkBool state expr.SourceSpan value
    | untyped.Break -> checkBreak state expr.SourceSpan
    | untyped.Bytes value -> checkBytes state expr.SourceSpan value
    | untyped.List elts -> checkList state expr.SourceSpan elts
    | untyped.Path path -> checkPath state expr.SourceSpan path

and private checkCollectionLiteral emptyType collDefinition toTypedExpr state span elts =
    match elts with
    | [] ->
        Compiled(
            { Type = emptyType
              Data = toTypedExpr []
              SourceSpan = span },
            []
        )
    | first :: rest ->
        compile {
            let! firstElt = checkExpression state first

            let! restElts =
                rest
                |> List.map (
                    checkExpression state
                    >> CompilerResult.satisfy (_.Type >> (=) firstElt.Type) (ifTypeMismatch firstElt.Type)
                )
                |> CompilerResult.sequence

            return
                { Type = EnjoyType.Generic(collDefinition, [ firstElt.Type ])
                  Data = toTypedExpr (firstElt :: restElts)
                  SourceSpan = span }
        }

and private checkArray state span elts =
    checkCollectionLiteral (EnjoyType.CLI typeof<obj[]>) typeof<Array> typed.Array state span elts

and private checkAssign state span recursive target value =
    failwith "FIXME"
    // match target with
    // | ExprPath(root, []) ->
    //     compile {
    //         let! value = checkExpression state value
    //         match root with
    //         | Var name ->
    //             match value.Type with
    //             | EnjoyType.Function(_, _, _) -> 
    //     }

and private checkAugAssign state span target op value = failwith "FIXME"

and private checkBinOp state span left op right = failwith "FIXME"

and private checkBool state span value =
    { Type = EnjoyType.CLI typeof<bool>
      Data = typed.Bool value
      SourceSpan = span }
    |> CompilerResult.singleton

and private checkBreak state span = failwith "FIXME"

and private checkBytes state span value =
    { Type = EnjoyType.CLI typeof<byte[]>
      Data = typed.Bytes value
      SourceSpan = span }
    |> CompilerResult.singleton

and private checkList state span elts =
    checkCollectionLiteral (EnjoyType.CLI typeof<ResizeArray<obj>>) typedefof<ResizeArray<_>> typed.List state span elts

and private checkPath state span (ExprPath(root, segments)) = failwith "FIXME"

let checkModule exprsCollection = failwith "FIXME"
