namespace Enjoy.Lang.Compiler

open System.Collections.Generic
open System.Reflection
open System.Reflection.Metadata;

type Closure =
    { Cells: IDictionary<string, EnjoyType>
      mutable InnerClosure: Closure option }

[<RequireQualifiedAccess>]
type TypedExpressionData =
    | And of values: TypedExpression list
    | AssignField of target: TypedExpression * field: FieldInfo * value: TypedExpression
    | AssignGlobal of name: string * value: TypedExpression
    | AssignLocal of target: string * value: TypedExpression
    | Array of elts: TypedExpression list
    | Await of value: TypedExpression
    | BinOp of left: TypedExpression * op: Operator * right: TypedExpression
    | Bool of value: bool
    | Bytes of value: byte[]
    | CodeBlock of body: TypedExpression list
    | Exprs of exprs: TypedExpression list
    | Field of value: TypedExpression * attr: FieldInfo
    | Float of value: float
    | For of init: TypedExpression * test: TypedExpression * step: TypedExpression * body: TypedExpression list
    | ForEach of target: string * iter: TypedExpression * body: TypedExpression list
    | FunctionCall of func: TypedExpression * posargs: TypedExpression list * kwargs: (string * TypedExpression) list
    | Global of name: string
    | If of test: TypedExpression * body: TypedExpression list * orelse: TypedExpression list
    | Int of value: int
    | Lambda of
        parameters: string list *
        defparams: (string * TypedExpression) list *
        locals: string list *
        desc: string *
        body: TypedExpression list *
        closurein: Closure option *
        closureout: Closure option
    | List of elts: TypedExpression list
    | Local of name: string
    | MethodCall of method: MethodInfo * args: TypedExpression list
    | Not of value: TypedExpression
    | Nothing
    | Object of entries: (string * TypedExpression) list
    | Operation of opcode: ILOpCode * values: TypedExpression list
    | Or of values: TypedExpression list
    | Repeat of times: TypedExpression * body: TypedExpression list
    | String of value: string
    | Switch of subject: TypedExpression * cases: TypedSwitchCase list * def: TypedExpression list
    | Tag of name: string * attrs: (string * TypedExpression) list * children: TypedExpression list
    | Try of body: TypedExpression list * handler: TypedExceptionHandler * finalbody: TypedExpression list
    | Until of test: TypedExpression * body: TypedExpression list
    | While of test: TypedExpression * body: TypedExpression list

and TypedExceptionHandler = TypedExceptionHandler of target: string * body: TypedExpression list

and TypedSwitchCase = TypedSwitchCase of value: SwitchValue * body: TypedExpression list

and TypedExpression =
    { Data: TypedExpressionData
      SourceSpan: SourceSpan
      Type: EnjoyType }

type typed = TypedExpressionData

module TypedExpression =
    let make data ``type`` =
        { Data = data
          SourceSpan = SourceSpan.zero
          Type = ``type`` }
