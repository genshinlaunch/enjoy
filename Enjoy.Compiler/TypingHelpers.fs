module Enjoy.Lang.Compiler.TypingHelpers

open System.Collections
open System.Reflection.Metadata;

open Util

let private numberCoercionPriorities =
    [ typeof<byte>
      typeof<sbyte>
      typeof<uint16>
      typeof<int16>
      typeof<uint>
      typeof<int>
      typeof<uint64>
      typeof<int64>
      typeof<float32>
      typeof<float> ]
    |> Seq.zip
    |> (|>) (Seq.initInfinite id)
    |> readOnlyDict

let isNumberType tp =
    numberCoercionPriorities.ContainsKey(tp)

let isIntType tp =
    isNumberType tp && tp <> typeof<float32> && tp <> typeof<float>

let private convertToBoolBySize value sizeProp =
    TypedExpression.make (typed.MethodCall(sizeProp, [ value ])) (EnjoyType.CLI typeof<int>)

let private implicitConvertToBool value =
    match value.Type with
    | EnjoyType.CLI t when t = typeof<bool> -> Some value.Data
    | EnjoyType.CLI t when t.IsAssignableTo(typeof<ICollection>) -> Some(typed.MethodCall(Symbols.count, [ value ]))
    | EnjoyType.CLI t when t = typeof<string> -> Some(typed.MethodCall(Symbols.length, [ value ]))
    | EnjoyType.CLI t when isNumberType t ->
        Some(
            typed.Operation(
                ILOpCode.Ceq,
                [ TypedExpression.make
                      (typed.Operation(
                          ILOpCode.Ceq,
                          [ value; TypedExpression.make (typed.Int 0) (EnjoyType.CLI typeof<int>) ]
                      ))
                      (EnjoyType.CLI typeof<int>)
                  TypedExpression.make (typed.Int 0) (EnjoyType.CLI typeof<int>) ]
            )
        )
    | EnjoyType.CLI t when t.IsValueType -> None
    | _ -> Some(typed.Exprs [ value; TypedExpression.make (typed.Int 1) (EnjoyType.CLI typeof<bool>) ])

let implicitConvert value ``type`` =
    match
        if value.Data = typed.Nothing then
            match ``type`` with
            | EnjoyType.CLI t when t = typeof<int> -> Some(typed.Int 0)
            | EnjoyType.CLI t when t = typeof<float> -> Some(typed.Float 0)
            | EnjoyType.CLI t when t = typeof<bool> -> Some(typed.Bool false)
            | EnjoyType.CLI t when t.IsValueType -> None
            | _ -> Some value.Data
        else
            match ``type`` with
            | EnjoyType.CLI t when t = typeof<obj> -> Some value.Data
            | EnjoyType.CLI t when t = typeof<string> -> Some(typed.MethodCall(Symbols.toString, [ value ]))
            | EnjoyType.CLI t when t = typeof<bool> -> implicitConvertToBool value
            | EnjoyType.Nothing -> raise Assert.Unreachable
            | _ -> Some typed.Nothing
    with
    | Some data ->
        Some
            { Type = ``type``
              Data = data
              SourceSpan = value.SourceSpan }
    | None -> None
