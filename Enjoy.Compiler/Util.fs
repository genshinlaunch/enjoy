module internal Enjoy.Lang.Compiler.Util

open System
open System.Collections.Generic

let initAndLast list =
    match List.rev list with
    | [] -> invalidArg (nameof (list)) "输入列表不可为空"
    | last :: initRev -> List.rev initRev, last

let joinStrings (separator: string) (strings: string seq) = String.Join(separator, strings)

let hexStringToBytes (hexString: string) =
    let hexString = hexString.Replace(" ", "")
    [| for i in 0 .. hexString.Length / 2 -> Convert.ToByte(hexString[i * 2 .. i * 2 + 1], 16) |]

let bytesToHexString (bytes: byte[]) =
    match bytes with
    | null -> ""
    | _ -> joinStrings "" <| seq { for i in bytes -> i.ToString("X2") }

let swap (x, y) = y, x

let takeLine (s: string) startat =
    String.Join("", s[startat..] |> Seq.takeWhile ((<>) '\n'))

let stringSkip startat (s: string) = s[startat..]

let getOrCreate key thunk (dict: IDictionary<_, _>) =
    match dict.TryGetValue(key) with
    | true, value -> value
    | false, _ ->
        let value = thunk ()
        dict.Add(key, value)
        value

let inline ifNull ([<InlineIfLambda>] ifNullThunk) value =
    match value with
    | null -> ifNullThunk ()
    | _ -> value

let sequence (options: _ seq) =
    let enumerator = options.GetEnumerator()

    let rec loop acc =
        if enumerator.MoveNext() then
            match enumerator.Current with
            | Some x -> loop (x :: acc)
            | None -> None
        else
            Some(List.rev acc)

    loop []

let mapO mapping xs =
    let rec loop xs acc =
        match xs with
        | [] -> Some(List.rev acc)
        | x :: xs ->
            match mapping x with
            | Some x -> loop xs (x :: acc)
            | None -> None

    loop xs []

type MaybeBuilder() =
    member _.Bind(x, f) =
        match x with
        | Some x -> f x
        | None -> None

    member _.Return(x) = Some x

    member _.ReturnFrom(x) = x

let maybe = MaybeBuilder()

let (|Eq|_|) expected input =
    if expected = input then Some Eq else None

let memoize fn =
    let cache = Dictionary()

    fun x ->
        match cache.TryGetValue(x) with
        | true, result -> result
        | false, _ ->
            let tmp = fn x
            cache.Add(x, tmp)
            tmp

module Assert =
    exception Unreachable
