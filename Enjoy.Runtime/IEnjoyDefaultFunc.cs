namespace Enjoy.Lang.Runtime;

// printf "public interface IEnjoyDefaultFunc<"
// for i = 1 to 14 do
//     printf "out T%d, " i
// printfn "out T15>\n{"
// for i = 1 to 15 do
//     printfn "    T%d? Default%d => default;" i i
// printfn "}"

public interface IEnjoyDefaultFunc<out T1, out T2, out T3, out T4, out T5, out T6, out T7, out T8, out T9, out T10, out T11, out T12, out T13, out T14, out T15>
{
    T1? Default1 => default;
    T2? Default2 => default;
    T3? Default3 => default;
    T4? Default4 => default;
    T5? Default5 => default;
    T6? Default6 => default;
    T7? Default7 => default;
    T8? Default8 => default;
    T9? Default9 => default;
    T10? Default10 => default;
    T11? Default11 => default;
    T12? Default12 => default;
    T13? Default13 => default;
    T14? Default14 => default;
    T15? Default15 => default;
}
