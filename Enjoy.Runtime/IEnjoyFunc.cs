namespace Enjoy.Lang.Runtime;

// printfn "public interface IEnjoyAction : IEnjoyDefaultFunc<%s>" (System.String.Join(", ", Seq.replicate 15 "object"))
// printfn "{"
// printfn "    void Invoke();"
// printfn "}\n"
// for i = 0 to 15 do
//     let defaultGenericArgs = (System.String.Join(", ",
//         [ for j in 1..i -> $"T{j}"
//           for j in i..14 -> "object"]))
//     let invokeArgs = (System.String.Join(", ", [ for j in 1..i -> $"T{j} x{j}"]))
//     if i > 0 then
//         printfn "public interface IEnjoyAction<%s> : IEnjoyDefaultFunc<%s>"
//             (System.String.Join(", ", [for j in 1..i -> $"T{j}"]))
//             defaultGenericArgs
//         printfn "{"
//         printfn "    void Invoke(%s)" invokeArgs
//         printfn "}\n"
//     printfn "public interface IEnjoyFunc<%s> : IEnjoyDefaultFunc<%s>"
//         (System.String.Join(", ",
//             [ for j in 1..i -> $"T{j}"
//               yield "out TResult" ]))
//         defaultGenericArgs
//     printfn "{"
//     printfn "    TResult Invoke(%s);" invokeArgs
//     printfn "}\n"

public interface IEnjoyAction : IEnjoyDefaultFunc<object, object, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke();
}

public interface IEnjoyFunc<out TResult> : IEnjoyDefaultFunc<object, object, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke();
}

public interface IEnjoyAction<T1> : IEnjoyDefaultFunc<T1, object, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1);
}

public interface IEnjoyFunc<T1, out TResult> : IEnjoyDefaultFunc<T1, object, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1);
}

public interface IEnjoyAction<T1, T2> : IEnjoyDefaultFunc<T1, T2, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2);
}

public interface IEnjoyFunc<T1, T2, out TResult> : IEnjoyDefaultFunc<T1, T2, object, object, object, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2);
}

public interface IEnjoyAction<T1, T2, T3> : IEnjoyDefaultFunc<T1, T2, T3, object, object, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3);
}

public interface IEnjoyFunc<T1, T2, T3, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, object, object, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3);
}

public interface IEnjoyAction<T1, T2, T3, T4> : IEnjoyDefaultFunc<T1, T2, T3, T4, object, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4);
}

public interface IEnjoyFunc<T1, T2, T3, T4, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, object, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, object, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, object, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, object, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, object, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, object, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, object, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, object, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, object, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, object, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, object, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, object, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, object, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, object, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, object, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, object, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, object, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, object, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, object, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, object>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13, T14 x14);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, object>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13, T14 x14);
}

public interface IEnjoyAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
{
    void Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13, T14 x14, T15 x15);
}

public interface IEnjoyFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, out TResult> : IEnjoyDefaultFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
{
    TResult Invoke(T1 x1, T2 x2, T3 x3, T4 x4, T5 x5, T6 x6, T7 x7, T8 x8, T9 x9, T10 x10, T11 x11, T12 x12, T13 x13, T14 x14, T15 x15);
}
