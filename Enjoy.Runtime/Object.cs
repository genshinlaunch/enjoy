namespace Enjoy.Lang.Runtime;

// for i = 1 to 15 do
//     let genericArgs = [ for j in 1..i -> $"T{j}" ]
//     let parameters = [ for j in 1..i -> $"T{j} Item{j}" ]
//     printfn "public record Object<%s>(%s);\n"
//         (String.Join(", ", genericArgs))
//         (String.Join(", ", parameters))

public record Object<T1>(T1 Item1);

public record Object<T1, T2>(T1 Item1, T2 Item2);

public record Object<T1, T2, T3>(T1 Item1, T2 Item2, T3 Item3);

public record Object<T1, T2, T3, T4>(T1 Item1, T2 Item2, T3 Item3, T4 Item4);

public record Object<T1, T2, T3, T4, T5>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5);

public record Object<T1, T2, T3, T4, T5, T6>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6);

public record Object<T1, T2, T3, T4, T5, T6, T7>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13, T14 Item14);

public record Object<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13, T14 Item14, T15 Item15);
