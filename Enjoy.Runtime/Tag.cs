﻿namespace Enjoy.Lang.Runtime;

public record Tag<T>(string Name, IDictionary<string, object> Attrs, IList<T> Children);
